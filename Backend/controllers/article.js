'use strict'

var validator = require('validator');
var Article = require('../models/article');

var fs = require('fs');
var path = require('path');

var controller = {
    datosCurso: (req, res) => {
        var hola = req.body.hola
        return res.status(200).send({
            curso : "Master en Frameworks",
            autor : "Javier Torres",
            url : "javier.com",
            hola
        });
    },

    test: (req, res) => {
        return res.status(200).send({
            message: 'Soy la acción test de mi controlador de articulos'
        });
    },

    save: (req, res) => {
        // Recoger parametros por POST
        var params = req.body;
        console.log(params);

        // Validar datos (validator)
        try{
            //
            var validate_title = !validator.isEmpty(params.title);
            var validate_content = !validator.isEmpty(params.content);
            
        }catch(err){
            return res.status(200).send({
                status: 'error',
                message : 'Faltan datos por Enviar !!!'
            });
        }
        if(validate_title && validate_content){
            
            // Crear el objeto a guardar
            var article = new Article();

            // Asignar valores
            article.title = params.title;
            article.content = params.content;

            if(params.image){
                article.image = params.image;
            }else{
                article.image = null;
            }
            

            // Guardar el artículo
            article.save((err, articleStored) => {

                if(err || !articleStored){
                    return res.status(404).send({
                        status: 'error',
                        message : 'El artículo no se ha guardado !!!'
                    });
                }
                else{
                    // Devolver una respuesta
                    return res.status(200).send({
                        status : 'success',
                        article : articleStored
                    });
                }

            });

            
        }
        else{
            return res.status(200).send({
                status: 'error',
                message : 'Los datos no son válidos'
            });
        }

        
    },
    getArticles : (req, res) =>{

        var query = Article.find({});

        // limit
        // Se usa para indicar cuantos resultados queremos que nos traiga la peticion
        var last = req.params.last;
        if(last || last != undefined){
            query.limit(5);
        }

        //find
        // VACIO CUANDO QUIERA EXTRAER TODO, ASI COMO SELECT * FROM

        // sort
        // La funcion 'sort' es para ordenar los elementos que traiga la consulta
        // '-criterio' es DESC Z-A
        // 'criterio' es ASC A-Z
        query.sort('-_id').exec((err,articles) =>{

            if(err){
                return res.status(500).send({
                    status: 'error',
                    message : 'Error al devolver articulos'
                });
            }

            if(!articles){
                return res.status(404).send({
                    status: 'error',
                    message : 'No hay artículos para mostrar'
                });
            }
            return res.status(200).send({
                status: 'success',
                articles
            });
        });
    },
    getArticle : (req, res) => {

        // Recoger el ID de la URL
        var articleId = req.params.id;

        //Comprobar que existe
        if(!articleId || articleId == null){
            return res.status(404).send({
                status: 'error',
                message : 'No existe'
            });
        }

        // Buscar el articulo
        Article.findById(articleId, (err, article) => {

            if(err || !article){
                return res.status(404).send({
                    status: 'error',
                    message : 'No existe el articulo'
                });
            }

            // Devolverlo en json
            return res.status(200).send({
                status: 'success',
                article
            });
        });
    },

    update: (req, res) => {
        //Recoger id del articulo por URL
        var articleId = req.params.id;

        // Recoger los datos que llegan por put
        var params = req.body;

        // validar los datos    
        try{
            var validate_title = !validator.isEmpty(params.title);
            var validate_content = !validator.isEmpty(params.content);

        }catch(err){
            return res.status(200).send({
                status: 'error',
                message : 'Faltan datos por enviar'
            });
        }
        
        if(validate_title && validate_content){
            //Find and Update
            // articleId = El id del obj a actualizar
            // params = Parametros nuevos que seran los que se actualizaran
            // new: true = Parametro de opciones, va a devolver el obj actualizado
            Article.findOneAndUpdate({_id: articleId}, params, {new :true}, (err, articleUpdated) => {
                if(err){
                    return res.status(500).send({
                        status: 'error',
                        message : 'Error al actualizar'
                    });
                }

                if(!articleUpdated){
                    return res.status(404).send({
                        status: 'error',
                        message : 'No Existe el Articulo'
                    });
                }
                return res.status(200).send({
                    status: 'success',
                    article: articleUpdated
                });

            });

        }else{
            // Devolver respuesta
            return res.status(200).send({
                status: 'error',
                message : 'La validacion no es correcta'
            });
        }
    },

    delete: (req, res) =>{
        // Recoger el id de la URL
        var articleId = req.params.id;
        

        // Find and delete
        Article.findOneAndDelete({_id: articleId}, (err, articleRemoved) =>{
            if(err){
                return res.status(500).send({
                    status: 'error',
                    message : 'Error al borrar'
                });
            }

            if(!articleRemoved){
                return res.status(404).send({
                    status: 'error',
                    message : 'No se ha borrado el articulo, posiblemente no exista'
                });
            }

            return res.status(200).send({
                status: 'success',
                article: articleRemoved
            });
        });
        
    },

    upload: (req, res) =>{
        // Configurar el modulo del connect multiparty router/article.js (Hecho)

        // Recoger el fichero de la peticion
        var file_name = 'Imagen no subida...';

        if(!req.files){
            return res.status(404).send({
                status : 'error',
                message : file_name
            });
        }

        // Conseguir nombre y extension del archivo
        var file_path = req.files.file0.path;

        // *** ADVERTENCIA EN WINDOWS ***
        //var file_split = file_path.split('\\');

        // *** ADVERTENCIA EN LINUX O MAC ***
        var file_split = file_path.split('/');

        // Nombre del Archivo
        var file_name = file_split[2];

        // Extension del fichero
        var extenion_split = file_name.split('\.');
        var file_ext = extenion_split[1];

        // Comprobar la extension, solo imagenes, si no es valida, borrar ficchero
        if(file_ext != 'png' && file_ext != 'jpg' && file_ext != 'jpeg' && file_ext != 'gif'){
            // unlink = Borrar archivo subido o fichero
            fs.unlink(file_path, (err) => {
                return res.status(200).send({
                    status : 'error',
                    message: 'La extensión de la imagen no es válida'
                });
            });

        }else{
            // Si todo es valido
            // Buscar articulo, asignarle el nombre de la imagen y actualizarlo
            var articleId = req.params.id;

            if(articleId){
                Article.findOneAndUpdate({_id: articleId}, {image: file_name}, {new: true}, (err, articleUpdated) => {
                
                    if(err || !articleUpdated){
                        return res.status(200).send({
                            status: 'err',
                            message: 'error al guardar la imagen del articulo'
                        });
                    }
                    
                    return res.status(200).send({
                        status: 'success',
                        article: articleUpdated
                    });
                });
            }else{
                return res.status(200).send({
                    status: 'success',
                    image: file_name
                });
            }
            
            
        } // end upload file

        
        
    },
    // TRAE LA IMAGEN DEL FICHERO
    getImage: (req, res) => {
        var file = req.params.image;

        var path_file = './upload/articles/'+file;

        fs.exists(path_file, (exists) => {
            if(exists){
                return res.sendFile(path.resolve(path_file));
            }else{
                return res.status(404).send({
                    status: 'error',
                    message: 'La imagen no existe'
                });
            }
        });
    },

    search : (req, res) =>{
        // Sacar el String a buscar
        var searchString = req.params.search;
        // Find or
        Article.find({"$or":[
            // SI el searchString esta I ncluido en el Titulo...
            { "title": { "$regex": searchString, "$options": "i"}},
            // SI el searchString esta I ncluido en el Content...
            { "content": { "$regex": searchString, "$options": "i"}}
        ]}) 
        // Va a ordenar por la FECHA de forma DESC
        .sort([['date', 'descending']])
        .exec((err, articles) => {

            if(err){
                return res.status(500).send({
                    status: 'error',
                    message: 'Error en la peticion'
                });
            }

            if(!articles || articles==""){
                return res.status(200).send({
                    status: 'error',
                    message: 'No hay articulos que coincidan con la bsuqueda'
                });
            }else{
                return res.status(200).send({
                    status: 'success',
                    articles
                });
            }

            
            
        });
    },
}; // end controler

module.exports = controller;