import { Component, OnInit } from '@angular/core';
import { Article } from '../../models/article';
import { ArticleService } from '../../services/article.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Global } from '../../services/global';
import swal from 'sweetalert';



@Component({
  selector: 'app-article-new',
  templateUrl: './article-new.component.html',
  styleUrls: ['./article-new.component.css'],
  providers: [ArticleService]
})
export class ArticleNewComponent implements OnInit {

  public article: Article;
  public status: string;
  public page_title:string;
  public is_edit: boolean;
  public url: string;
  

  afuConfig = {
    multiple: false,
    formatsAllowed: ".jpg,.png, .gif, .jpeg",
    maxSize: "50",
    uploadAPI:  {
      url: Global.url+'upload-image'
    },
    // theme: atachPin se pone manuamente porque es el formulario mas sencillo
    // Por default tiene dragNDrop
    theme: "attachPin",
    hideProgressBar: true,
    hideResetBtn: true,
    hideSelectBtn: false,
    replaceTexts: {
      selectFileBtn: 'Select Files',
      resetBtn: 'Reset',
      uploadBtn: 'Upload',
      dragNDropBox: 'Drag N Drop',
      attachPinBtn: 'Sube tu imagen...',
      afterUploadMsg_success: 'Successfully Uploaded !',
      afterUploadMsg_error: 'Upload Failed !'
    }
  };

  constructor(
    private _articleService: ArticleService,
    private _router: Router,
    private _route: ActivatedRoute
  ) { 
    this.article = new Article('','','',null, null);
    this.page_title = "Crear Articulo";
    this.is_edit = false;
    this.url = Global.url;
  }

  ngOnInit(): void {
  }

  onSubmit(){
   this._articleService.create(this.article).subscribe(
     response => {
      if(response.status=="success"){
        this.status="success";
        this.article=response.article;
        this._router.navigate(['/blog']);
        //Alerta
        swal(
          'Articulo creado!',
          'El articulo se ha creado correctamente',
          'success'
        );
      }
      else{
        this.status="error";
      }
     },
     error => {
       console.log(error);
       this.status = error;
     }
   )
  }

  imageUpload(data){
    let image_data = JSON.parse(data.response);
    this.article.image = image_data.image;
  }
}
