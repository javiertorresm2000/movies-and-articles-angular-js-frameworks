import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ArticleService} from '../../services/article.service';
import {Article} from '../../models/article';
import { Response } from 'selenium-webdriver/http';
import { Global } from 'src/app/services/global';
import swal from 'sweetalert';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css'],
  providers: [ArticleService],
})
export class ArticleComponent implements OnInit {
public article: Article;
public url: string
  constructor(
    private _articleService : ArticleService,
    private _route: ActivatedRoute,
    private _router: Router
  ) {
    this.url = Global.url;
  }

  ngOnInit(): void {
    this._route.params.subscribe( params =>{
      let id = params['id'];

      this._articleService.getArticle(id).subscribe(
        response =>{
          if(response.article){
            this.article = response.article
          }else{
            this._router.navigate(['/home']);
          }
        },
        error =>{
          this._router.navigate(['/home']);
        }
      )
    })
    
  }

  delete(id){

    swal({
      title: "Estás seguro?",
      text: "Una vez borrado el articulo no podrás recuperarlo!",
      icon: "warning",
      buttons: [true, true],
      dangerMode: true
    })
    .then((willDelete) => {
      if (willDelete) {
        this._articleService.delete(id).subscribe(
          response => {
            swal("Borrado con Éxito","El articulo ha sido borrado", 'success');
            this._router.navigate(['/blog']);
          },
          error => {
            console.log(error);
            swal("Ocurrió un Error","Error al borrar el artículo", 'error');
          }
        )
      } else {
        swal("Tranquilo, no la has cagado jeje");
      }
    });

    
  }

}
