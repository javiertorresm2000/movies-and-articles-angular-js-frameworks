import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {
  public user: any;
  public campo: string;
  public campo2: string;

  constructor() {
    this.user = {
      nombre: '',
      apellidos: '',
      bio: '',
      genero: ''
    };
  }

  ngOnInit(): void {
  }

  onSubmit(){
    alert("Form Enviado");
    console.log(this.user)
  }

  hasDadoClick(){
    alert("Click");
  }

  hasSalido(){
    alert("Saliste");
  }

  hasDadoEnter(){
    alert("HasDadoEnter");
  }

}
