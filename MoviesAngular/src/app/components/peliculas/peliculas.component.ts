import { Component, OnInit, DoCheck, OnDestroy } from '@angular/core';
import { Pelicula } from '../../models/pelicula';
import { PeliculaService } from '../../services/pelicula.service';

@Component({
  selector: 'peliculas',
  templateUrl: './peliculas.component.html',
  styleUrls: ['./peliculas.component.css'],
  providers: [PeliculaService],
})
export class PeliculasComponent implements OnInit, DoCheck, OnDestroy {
  public titulo: string;
  public peliculas: Pelicula[];
  public favorita: Pelicula;
  public fecha: any;

  constructor(
    // '_' siempre se pone a una propiedad que esta vinculada a un servicio
    private _peliculaService: PeliculaService
  ) { 
    this.titulo = "Componente Peliculas";
    this.peliculas = this._peliculaService.getPeliculas();
      this.fecha= new Date(2020,8,12);
  }
  

  ngOnInit(): void {
    console.log(this._peliculaService.holaMundo)
  }

  ngDoCheck(){
  }

  cambiarTitulo(){
  }

  ngOnDestroy(){
  }

  mostrarFavorita(event){
    this.favorita = event.pelicula;
  }

}
