import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {
  // Con INPUT agarramos datos que mandamos con una directiva desde un html
  @Input() nombre: string; 
  @Input() size: string;


  constructor() { }

  ngOnInit(): void {
  }

}
