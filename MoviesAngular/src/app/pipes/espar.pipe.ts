import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'espar'
})

export class EsParPipe implements PipeTransform{
    transform(value: any){
        var espar="No es PAR";
        if(value % 2 == 0){
            espar = "Es PAR"
        }
        return "El año es:"+value+" y "+espar;
    }
}