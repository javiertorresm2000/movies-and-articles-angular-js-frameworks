import { Injectable } from '@angular/core';
import { Pelicula } from '../models/pelicula';


@Injectable()

    
export class PeliculaService{
    public peliculas: Pelicula[]
    constructor(){
        this.peliculas = [
            new Pelicula("Spiderman 4", 2019,'https://cdn.shortpixel.ai/client/q_lossy,ret_img,w_600/https://www.elgoldigital.com/wp-content/uploads/ara%C3%B1a-11-600x400.jpg'),
            new Pelicula("Los Vengadores End Game", 2018,'https://as.com/tikitakas/imagenes/2019/04/26/portada/1556258369_131914_1556258703_noticia_normal.jpg'),
            new Pelicula("Batman vs Superman", 2015,'https://img.europapress.es/fotoweb/fotonoticia_20140816120630_1024.jpg'),
            ];
    }
    holaMundo(){
        return "Hola mundo";
    }

    getPeliculas(){
        return this.peliculas
    }
}